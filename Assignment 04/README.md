# Database Design for Pokémon: A Comparison of SQL and NoSQL Implementations using Gen-AI

## Introduction

This repository contains the report for Assignment 04 Gen-AI by Tuhina Tripathi. The report analyzes the process of designing a database for managing Pokémon data using both SQL and NoSQL approaches with the assistance of GenAI. It also compares the experiences and observations of working with both SQL and NoSQL databases.

## Problem Statement

The objective of this assignment was to design a database for managing Pokémon data, incorporating both SQL and NoSQL approaches, and to evaluate the assistance provided by GenAI in the process. The specific requirements included creating a relational database schema, populating the tables, implementing a many-to-many relationship, and writing queries to retrieve specific information.

## NoSQL: MongoDB

This section discusses the experience of working with MongoDB, including setting up the database, populating tables, and writing queries. It also provides observations and comparisons with SQL.

## SQL

This section discusses the experience of working with SQL (PostgreSQL), including setting up the database, populating tables, and writing queries. It also provides observations and comparisons with NoSQL.

## Comparison of SQL vs NoSQL

This section compares the experiences of working with SQL and NoSQL databases, highlighting their strengths and weaknesses. It also discusses the efficiency of GenAI in assisting with both types of databases and suggests areas for improvement.

## Conclusion

In conclusion, the report summarizes the findings of the analysis and emphasizes the importance of understanding the differences between SQL and NoSQL databases. It also acknowledges the challenges faced in working with GenAI and suggests ways to improve its support for MySQL databases.

- [Conversation Link 1](https://g.co/gemini/share/26e71ea3c122)
- [Conversation Link 2](https://g.co/gemini/share/d973a69da58a)
- [Conversation Link 3](https://g.co/gemini/share/0f47ee825857)
- [Conversation Link 4](https://g.co/gemini/share/0254dc961e91)

