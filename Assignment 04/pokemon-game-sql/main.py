import pygame
import pygame.image
import pymongo



# Initialize Pygame
pygame.init()

# Set display size and caption
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("My Pokemon Game")

# Set background color (optional)
background_color = (0, 0, 100)  # This is dark blue

# Load background image
background_image = pygame.image.load("./img/umbrion.jpeg")

# Get the screen size
screen_width, screen_height = screen.get_size()

# Scale the image to fit the screen (preserves aspect ratio)
scaled_image = pygame.transform.scale(background_image, (screen_width, screen_height))

# Blit the scaled image
screen.blit(scaled_image, (0, 0))

# Game loop
running = True
while running:
    # Handle events (user input)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the screen with background color (optional, if needed)
    # screen.fill(background_color)

    # Update game objects (logic)

    # Draw graphics to the screen
    # (code to draw characters, etc.) on top of the background

    # Update the display
    pygame.display.flip()

# Quit Pygame
pygame.quit()
