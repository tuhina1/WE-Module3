import psycopg2

# Database connection details (replace with your credentials)
dbname = "postgres"
user = "postgres"
password = "postgres"
host = "localhost"  # Modify if your database is on a different machine
port = 5432  # Default port for PostgreSQL

# Connect to the database
try:
  connection = psycopg2.connect(database=dbname, user=user, password=password, host=host, port=port)
  cursor = connection.cursor()
  print("Connected to PostgreSQL database!")
except psycopg2.Error as e:
  print("Error connecting to PostgreSQL database:", e)
  exit()

def create_pokemon_table():
  """Creates the pokemon table and populates it with sample data."""
  cursor.execute("""
    CREATE TABLE pokemon_table_2 (
      id SERIAL PRIMARY KEY,
      name VARCHAR(50) NOT NULL UNIQUE,
      type1 VARCHAR(20) NOT NULL,
      type2 VARCHAR(20),
      hp INTEGER,
      attack INTEGER,
      defense INTEGER,
      sp_atk INTEGER,
      sp_def INTEGER,
      speed INTEGER 
    );
  """)
  connection.commit()
  print("Created pokemon table.")

  # Insert sample Pokemon data
  pokemon_data = [
      ("Bulbasaur", "Grass", None, 45, 49, 49, 65, 65, 45),
      ("Charmander", "Fire", None, 39, 52, 43, 60, 50, 65),
      ("Squirtle", "Water", None, 44, 48, 50, 50, 64, 43),
      ("Eevee", "Normal", None, 55, 55, 55, 49, 55, 55),
      ("Pidgey", "Normal", "Flying", 40, 45, 40, 50, 35, 56),
  ]
  for name, type1, type2, hp, attack, defense, sp_atk, sp_def, speed in pokemon_data:
      cursor.execute("""
          INSERT INTO pokemon_table_2 (name, type1, type2, hp, attack, defense, sp_atk, sp_def, speed)
          VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
      """, (name, type1, type2, hp, attack, defense, sp_atk, sp_def, speed))
  connection.commit()
  print("Populated pokemon table with sample data.")

def create_moves_table():
  """Creates the moves table and populates it with sample data."""
  cursor.execute("""
    CREATE TABLE moves_table_2 (
      id SERIAL PRIMARY KEY,
      name VARCHAR(50) NOT NULL UNIQUE,
      power INTEGER,
      accuracy INTEGER,
      type VARCHAR(20) NOT NULL,
      pp INTEGER NOT NULL
    );
  """)
  connection.commit()
  print("Created moves table.")

  # Insert sample move data
  move_data = [
      ("Tackle", 35, 95, "Normal", 35),
      ("Water Gun", 40, 100, "Water", 25),
      ("Ember", 40, 100, "Fire", 25),
      ("Vine Whip", 40, 100, "Grass", 25),
      ("Wing Attack", 65, 100, "Flying", 35),
      ("Headbutt", 70, 100, "Normal", 15),
      ("Return", 100, 100, "Normal", 20),
  ]
  for name, power, accuracy, type, pp in move_data:
      cursor.execute("""
          INSERT INTO moves_table_2 (name, power, accuracy, type, pp)
          VALUES (%s, %s, %s, %s, %s)
      """, (name, power, accuracy, type, pp))
  connection.commit()
  print("Populated moves table with sample data.")

def create_pokemon_moves_table():
  """Creates the pokemon_moves table."""
  cursor.execute("""
    CREATE TABLE pokemon_moves_table_2 (
      pokemon_id INTEGER NOT NULL REFERENCES pokemon_table_2(id),
      move_id INTEGER NOT NULL REFERENCES moves_table_2(id),
      PRIMARY KEY (pokemon_id, move_id)
    );
  """)
  connection.commit()
  print("Created pokemon_moves table.")

  # Insert sample Pokemon move relationships
  pokemon_moves_data = [
      (1, 1),  # Bulbasaur - Tackle
      (1, 2),  # Bulbasaur - Vine Whip
      (1, 7),  # Bulbasaur - Return
      (2, 1),  # Charmander - Tackle
      (2, 3),  # Charmander - Ember
      (2, 7),  # Charmander - Return
      (3, 1),  # Squirtle - Tackle
      (3, 4),  # Squirtle - Water Gun
      (3, 7),  # Squirtle - Return
      (4, 1),  # Eevee - Tackle
      (4, 5),  # Eevee - Headbutt
      (4, 7),  # Eevee - Return
      (5, 1),  # Pidgey - Tackle
      (5, 6),  # Pidgey - Wing Attack
      (5, 7),  # Pidgey - Return
  ]
  for pokemon_id, move_id in pokemon_moves_data:
      cursor.execute("""
          INSERT INTO pokemon_moves_table_2 (pokemon_id, move_id)
          VALUES (%s, %s)
      """, (pokemon_id, move_id))
  connection.commit()
  print("Populated pokemon_moves table with sample data.")
  print("Created pokemon_moves table.")

# Call the functions to create and populate tables
# create_pokemon_table()
# create_moves_table()
# create_pokemon_moves_table()

def get_pokemon_with_move(cursor, move_name):
  """
  This function retrieves Pokemon that can learn a specific move.

  Args:
      cursor: A database cursor object.
      move_name: The name of the move to search for.

  Returns:
      A list of Pokemon names that can learn the specified move.
  """
  cursor.execute("""
  SELECT p.name
  FROM pokemon_table_2 p
  INNER JOIN pokemon_moves_table_2 pm ON p.id = pm.pokemon_id
  INNER JOIN moves_table_2 m ON pm.move_id = m.id
  WHERE m.name = %s;
  """, (move_name,))
  results = cursor.fetchall()
  pokemon_list = [row[0] for row in results]  # Extract Pokemon names
  return pokemon_list

# # Example usage
# move_to_find = "Return"
# pokemon_with_return = get_pokemon_with_move(cursor, move_to_find)

# if pokemon_with_return:
#   print(f"Pokemon that can learn {move_to_find}:")
#   for pokemon in pokemon_with_return:
#     print(pokemon)
# else:
#   print(f"No Pokemon found that can learn {move_to_find}.")

def get_moves_against_type(cursor, type_name):
  """
  This function retrieves moves that are powerful against a specific type.

  Args:
      cursor: A database cursor object.
      type_name: The name of the type to search for.

  Returns:
      A list of move names that are powerful against the specified type.
  """
  cursor.execute("""
  SELECT m.name
  FROM moves_table_2 m
  INNER JOIN pokemon_types_table pt ON m.type = pt.type_name
  WHERE pt.effectiveness_against = %s;
  """, (type_name,))
  results = cursor.fetchall()
  move_list = [row[0] for row in results]  # Extract move names
  return move_list

# # Example usage
# type_to_check = "Grass"
# moves_against_grass = get_moves_against_type(cursor, type_to_check)

# if moves_against_grass:
#   print(f"Moves powerful against {type_to_check} type:")
#   for move in moves_against_grass:
#     print(move)
# else:
#   print(f"No moves found that are powerful against {type_to_check} type.")

def create_type_effectiveness_table():
  """Creates the type_effectiveness table."""
  cursor.execute("""
    CREATE TABLE type_effectiveness_1 (
      type_name VARCHAR(20) PRIMARY KEY,
      weakness_to VARCHAR(20),
      effectiveness_against VARCHAR(20)
    );
  """)
  connection.commit()
  print("Created type_effectiveness table.")

def populate_type_effectiveness_table():
  """Populates the type_effectiveness table with sample data."""
  type_data = {
      "Fire": {"weakness_to": "Water", "effectiveness_against": "Grass"},
      "Grass": {"weakness_to": ["Fire", "Flying"], "effectiveness_against": "Water"},
      "Water": {"weakness_to": "Grass", "effectiveness_against": "Fire"},
      "Normal": {"weakness_to": None, "effectiveness_against": None},
      "Flying": {"weakness_to": None, "effectiveness_against": "Grass"},
  }
  for type_name, info in type_data.items():
      weakness = info.get("weakness_to")  # Get weakness (might be None, list, or string)
      effectiveness = info.get("effectiveness_against")
      cursor.execute("""
          INSERT INTO type_effectiveness_1 (type_name, weakness_to, effectiveness_against)
          VALUES (%s, %s, %s)
      """, (type_name, weakness, effectiveness))
  connection.commit()
  print("Populated type_effectiveness table with sample data.")

def get_moves_against_type(cursor, type_name):
  """
  This function retrieves moves that are powerful against a specific type.

  Args:
      cursor: A database cursor object.
      type_name: The name of the type to search for.

  Returns:
      A list of move names that are powerful against the specified type.
  """
  cursor.execute("""
  SELECT m.name
  FROM moves_table_2 m
  INNER JOIN type_effectiveness_1 te ON m.type = te.type_name
  WHERE te.effectiveness_against = %s;
  """, (type_name,))
  results = cursor.fetchall()
  move_list = [row[0] for row in results]  # Extract move names
  return move_list

# Example usage
type_to_check = "Grass"
moves_against_grass = get_moves_against_type(cursor, type_to_check)

if moves_against_grass:
  print(f"Moves powerful against {type_to_check} type:")
  for move in moves_against_grass:
    print(move)
else:
  print(f"No moves found that are powerful against {type_to_check} type.")
