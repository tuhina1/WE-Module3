import pymongo 

try:
    # Replace with your connection URI
    connection_uri = "mongodb+srv://<username>:<password>@cluster0.abcde.mongodb.net/test?retryWrites=true&w=majority"
    client = pymongo.MongoClient(connection_uri)
    # Force a ping to check authentication (may throw exception)
    client.admin.command("ping")
    print("Connected to MongoDB successfully!")

except pymongo.errors.ConnectionFailure as e:
  print("Connection error:", e)

db = client["Cluster0"]

# Pokemon Collection
pokemon_collection = db["pokemon"]

# Filter for Grass-type Pokemon with moves of type "Fire" or "Flying"
cursor = pokemon_collection.find({
    "type": "Grass",
    "moves": {
        "$elemMatch": {
            "type": { "$in": ["Fire", "Flying"] }
        }
    }
})

# Iterate through results and print Pokemon names (or desired details)
for pokemon in cursor:
    print(pokemon["name"])  # Assuming "name" field stores the Pokemon name
    # Access other details from the Pokemon document as needed
    
# Close connection (optional)
client.close()
