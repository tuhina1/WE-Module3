class Move:
  def __init__(self, name, power, type, learnable_by=[]):
    self.name = name
    self.power = power  # Base power of the move
    self.type = type
    self.learnable_by = learnable_by  # List of Pokemon names (strings) that can learn this move

  @classmethod
  def get_move_object(cls, move_name):
    """Retrieves the Move object for the given move name (if it exists).

    Args:
        move_name (str): The name of the move to retrieve.

    Returns:
        Move object: The Move object associated with the name, or None if not found.
    """

    # Replace with your implementation to access move data
    # This example uses a hypothetical move_data dictionary
    move_data = {  # Hypothetical dictionary containing Move objects
        "Tackle": Move("Tackle", 50, "Normal", ["Pikachu", "Bulbasaur"]),
        "Ember": Move("Ember", 40, "Fire", ["Charmander", "Growlithe"]),
    }
    return move_data.get(move_name)
