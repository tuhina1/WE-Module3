import pymongo

try:
    # Replace with your connection URI
    connection_uri = "mongodb+srv://<username>:<password>@cluster0.abcde.mongodb.net/test?retryWrites=true&w=majority"
    client = pymongo.MongoClient(connection_uri)
    # Force a ping to check authentication (may throw exception)
    client.admin.command("ping")
    print("Connected to MongoDB successfully!")

except pymongo.errors.ConnectionFailure as e:
  print("Connection error:", e)

db = client["Cluster0"]

# Define Pokemon Collection (schema)
pokemon_collection = db["pokemon"]

# Define Move Collection (schema)
move_collection = db["moves"]

# Pokemon Data
pokemon_data = [
    {"name": "Bulbasaur", "type": "Grass", "moves": ["Tackle", "Vine Whip", "Return"]},
    {"name": "Charmander", "type": "Fire", "moves": ["Tackle", "Ember", "Return"]},
    {"name": "Squirtle", "type": "Water", "moves": ["Tackle", "Water Gun", "Return"]},
    {"name": "Eevee", "type": "Normal", "moves": ["Tackle", "Headbutt", "Return"]},
    {"name": "Pidgey", "type": "Normal/Flying", "moves": ["Tackle", "Wing Attack", "Return"]},
]
pokemon_collection.insert_many(pokemon_data)

# Move Data (assuming separate Move document)
move_data = [
    {"name": "Tackle", "type": "Normal", "power": 35},
    {"name": "Water Gun", "type": "Water", "power": 40},
    {"name": "Ember", "type": "Fire", "power": 40},
    {"name": "Vine Whip", "type": "Grass", "power": 40},
    {"name": "Wing Attack", "type": "Flying", "power": 65},
    {"name": "Headbutt", "type": "Normal", "power": 70},
    {"name": "Return", "type": "Normal", "power": 100},
]
move_collection.insert_many(move_data)

# Print success message
print("Pokemon and Move data inserted successfully!")

# Close connection (optional)
client.close()
