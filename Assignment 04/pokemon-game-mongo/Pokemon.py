import pygame
from .Move import Move

class Pokemon:
  def __init__(self, name, primary_type, secondary_type=None, hp=100, attack=50, defense=50, sp_attack=50, sp_defense=50, speed=50, moves=[], image_path=None):
    self.name = name
    self.primary_type = primary_type
    self.secondary_type = secondary_type
    self.stats = {
      "hp": hp,
      "attack": attack,
      "defense": defense,
      "sp_attack": sp_attack,
      "sp_defense": sp_defense,
      "speed": speed
    }
    self.moves = moves  # List of move objects
    self.current_hp = hp
    self.image = None  # Load image later if path provided

    if image_path:
      self.image = pygame.image.load(image_path)

  # Add methods for Pokemon actions (attack, take damage, etc.)
  # ...

  def get_types(self):
    """Returns a list of the Pokemon's types."""
    types = [self.primary_type]
    if self.secondary_type:
      types.append(self.secondary_type)
    return types
  
  def can_learn(self, move_name):
    """Checks if the Pokemon can learn the specified move."""
    return move_name in self.learnable_moves

  def learn_move(self, move_name):
    """Adds the move to the Pokemon's known moves if it can learn it."""
    if self.can_learn(move_name):
      # Find the Move object from a central data source (replace with your implementation)
      move_object = Move.get_move_object(move_name)  # Replace with your function to retrieve the Move object
      if move_object:
        self.moves.append(move_object)

