import pymongo 

try:
    # Replace with your connection URI
    connection_uri = "mongodb+srv://<username>:<password>@cluster0.abcde.mongodb.net/test?retryWrites=true&w=majority"
    client = pymongo.MongoClient(connection_uri)
    # Force a ping to check authentication (may throw exception)
    client.admin.command("ping")
    print("Connected to MongoDB successfully!")

except pymongo.errors.ConnectionFailure as e:
  print("Connection error:", e)

db = client["Cluster0"]

# Pokemon Collection
pokemon_collection = db["pokemon"]

# Find Pokemon with "Return" in moves list
query = {"moves": {"$in": ["Return"]}}
result_cursor = pokemon_collection.find(query)

# Print Pokemon names
for pokemon in result_cursor:
    print(pokemon["name"])

# Close connection (optional)
client.close()
