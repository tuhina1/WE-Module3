from Suite import Suite
class DiamondSuite(Suite):
    """Subclass of Suite specifically for Diamonds (optional, but demonstrates inheritance)."""
    def __init__(self):
        super().__init__("Diamonds", (255, 165, 0))  # Inherit name and color from Suite
