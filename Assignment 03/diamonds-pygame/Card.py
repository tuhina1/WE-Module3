from Suite import Suite
class Card:
    """Represents a single playing card with a suite and rank."""
    RANKS = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "J", "Q", "K"]
    SUITES = [Suite("H", (255, 0, 0)), Suite("S", (0, 0, 0)),
              Suite("D", (255, 165, 0)), Suite("C", (0, 128, 0))]

    def __init__(self, rank, suite):
        if rank not in Card.RANKS or suite not in Card.SUITES:
            raise ValueError("Invalid rank or suite")
        self.rank = rank
        self.suite = suite

    def get_value(self):
        """Returns the card's value (e.g., face value, blackjack value)."""
        # Implement your specific logic for card value based on game rules
        # This example uses face value for simplicity
        if self.rank in ["J", "Q", "K"]:
            return 10
        else:
            try:
                return int(self.rank)
            except ValueError:
                return 1  # Handle Ace as 1 (adjust based on game rules)
