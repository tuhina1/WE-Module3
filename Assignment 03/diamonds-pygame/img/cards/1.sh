svg_files=$(find . -name "*.svg")
for svg_file in $svg_files; do
  inkscape --export-png="${svg_file%.*}.png" "$svg_file"
done
