
class Suite:
    """Represents a card suite (e.g., Hearts, Spades, Diamonds, Clubs)."""
    def __init__(self, name, color):
        self.name = name
        self.color = color
