import random

from Card import Card
class Game:
  """Represents the game state, including the deck, players, etc."""
  def __init__(self):
    self.deck = [Card(rank, suite) for rank in Card.RANKS for suite in Card.SUITES]
    self.players = []  # List to store players in the game
    self.selected_card = None  # Stores the currently selected card object (initially None)
    self.discarded_cards = []
  def deal_cards(self, num_cards_per_player):
    """Deals cards to each player in the game."""
    for player in self.players:
      for _ in range(num_cards_per_player):
        player.draw_card(self.deck)

  def play_round(self):
    """Conducts a single round of the game."""

    # Pick a random suit
    chosen_suit = random.choice(Card.SUITES)
    print(f"Selected suit for this round: {chosen_suit.name}")

    # Loop through players to select cards (automatic)
    for player in self.players:
      matching_cards = [card for card in player.hand if card.suite == chosen_suit]
      if matching_cards:
        # Automatically select the first matching card
        selected_card = matching_cards[0]
        player.hand.remove(selected_card)
        self.discarded_cards.append(selected_card)
        print(f"{player.name} plays {selected_card.rank}{selected_card.suite.name}")
      else:
        # Player has no matching cards, other player wins
        winner = next(p for p in self.players if p != player)
        print(f"{player.name} has no cards matching the {chosen_suit.name} suit. {winner.name} wins the round!")
        return winner.name
