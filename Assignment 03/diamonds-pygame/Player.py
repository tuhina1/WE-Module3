class Player:
    """Represents a player in the card game."""
    def __init__(self, name):
        self.name = name
        self.hand = []  # List to store cards in the player's hand

    def draw_card(self, deck):
        """Draws a card from the deck and adds it to the player's hand."""
        card = deck.pop()
        self.hand.append(card)

    def discard_card(self, index):
        """Discards a card from the player's hand at the specified index."""
        if 0 <= index < len(self.hand):
            return self.hand.pop(index)
        else:
            raise IndexError("Invalid card index") 