import pygame

class CardButton(pygame.sprite.Sprite):
  def __init__(self, card, x, y):
    super().__init__()
    self.card = card
    self.image = card.image  # Use the card's loaded image
    self.rect = self.image.get_rect(topleft=(x, y))  # Set button position

  def handle_click(self, game):
    """Handles a click on the button, selecting the card."""
    if not game.selected_card:
      game.selected_card = self.card
