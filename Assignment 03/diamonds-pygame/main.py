import pygame
import colorsys
import os 
from Player import Player
from Game import Game
from CardButton import CardButton
       
base_color = (0, 0, 128)
h, s, v = colorsys.rgb_to_hsv(*base_color)
new_saturation = s * 0.7 
new_color = colorsys.hsv_to_rgb(h, new_saturation, v)

# Define some colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# Initialize Pygame
pygame.init()

# Set the screen size
width = 800
height = 600
screen = pygame.display.set_mode((width, height))

# Set the window title
pygame.display.set_caption("My Card Game")

# Load the background image
background_image = pygame.image.load("img/bg.jpeg") # Replace with your image path
background_image = pygame.transform.scale(background_image, (width, height))

if not background_image.get_alpha():
  background_image = background_image.convert_alpha() 

transparency_level = 128  
background_image.set_alpha(transparency_level)

card_image_folder = "img/cards/" 
game = Game()
game.players.append(Player("Alice"))
game.players.append(Player("Bob"))
game.deal_cards(4)

card_buttons = []
for player in game.players:
  for i in range(len(player.hand)):
    card = player.hand[i]

    image_path = os.path.join(card_image_folder, f"{card.rank}{card.suite.name}.png")
    card.image = pygame.image.load(image_path)  # Reload image on each iteration

    card_width = card.image.get_width()
    card_height = card.image.get_height()

    # Calculate hand position based on player
    hand_start_x = (width - (len(player.hand) * card_width + (len(player.hand) - 1) * 50)) // 2
    button_x = hand_start_x + i * (card_width + 50)
    button_y = height - card_height - 10

    # Create a CardButton and add it to the list
    card_button = CardButton(card, button_x, button_y)
    card_buttons.append(card_button)

pygame.font.init() 
my_font = pygame.font.SysFont('Comic Sans MS', 30)

# Game loop
running = True
while running:
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False

    if event.type == pygame.MOUSEBUTTONDOWN:
      mouse_x, mouse_y = pygame.mouse.get_pos()
      for button in card_buttons:
        if button.rect.collidepoint(mouse_x, mouse_y):
          button.handle_click(game)  # Handle click on the button

     # Call the play_round function when a round is complete (e.g., after a card selection)
    if game.selected_card:  # Replace with your condition to check for round completion
        winner = game.play_round()
        # Render some text
        text_surface = my_font.render(winner, False, (0, 0, 0))
        screen.blit(text_surface, (400, 400))
        game.selected_card = None  # Reset selected card for the next round


    # Draw the background image
    screen.fill(new_color)
    screen.blit(background_image, (0, 0), area=(0, 0, width, height))

    # Draw game elements (e.g., cards, buttons) on top of the background
    # Example: Draw player hands
    card_width = 60  # Adjust card width and height for smaller size
    card_height = 90  # Adjust card height as needed
    card_spacing = 10  # Space between cards in hand

    # Calculate starting x position for the hand (centered)
    hand_start_x_1 = ((width - (len(game.players[0].hand) * card_width + (len(game.players[0].hand) - 1) * card_spacing)) // 2) - 300

    # Draw player 1's hand
    for i in range(len(game.players[0].hand)):
        card = game.players[0].hand[i]
        image_path = os.path.join(card_image_folder, f"{card.rank}{card.suite.name}.png")
        card.image = pygame.image.load(image_path)  # Reload image on each iteration
        x_pos = hand_start_x_1 + i * (card_width + card_spacing)
        y_pos = height - card_height - 10  # Adjust y position for bottom placement

        # Scale the card image if necessary (adjust scaling factor)
        scaled_image = pygame.transform.scale(card.image, (card_width, card_height))
        screen.blit(scaled_image, (x_pos, y_pos))

    # Calculate starting x position for the hand (centered)
    hand_start_x_2 = 300 + (width - (len(game.players[1].hand) * card_width + (len(game.players[1].hand) - 1) * card_spacing)) // 2
    # Draw player 2's hand
    for i in range(len(game.players[1].hand)):
        card = game.players[1].hand[i]
        image_path = os.path.join(card_image_folder, f"{card.rank}{card.suite.name}.png")
        card.image = pygame.image.load(image_path)  # Reload image on each iteration
        x_pos = hand_start_x_2 + i * (card_width + card_spacing)
        y_pos = height - card_height - 10  # Adjust y position for bottom placement

        # Scale the card image if necessary (adjust scaling factor)
        scaled_image = pygame.transform.scale(card.image, (card_width, card_height))
        screen.blit(scaled_image, (x_pos, y_pos))
  # Update game logic here (e.g., card handling, player actions)
  # Draw game elements (e.g., cards, buttons) on top of the background

  # Update the display
  pygame.display.flip()

# Quit Pygame
pygame.quit()
