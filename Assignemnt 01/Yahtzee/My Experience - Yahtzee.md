# My Experience - Yahtzee

## **Understanding Yahtzee Game:**

So, I asked ChatGPT what Yahtzee is all about. And I got to know that Yahtzee is a dice game where players take turns rolling five dice with the goal of scoring points in various categories. The game involves strategic decision-making and a bit of luck, as players aim to maximize their scores by achieving specific combinations. Very Straightforward and simple answer. Sufficiently answered my question.

## **Playing Yahtzee:**

I wanted to play with game with the Gen-AI, however it refused to me initially by stating, as it is a text-based AI, It is unable to directly play games with me in real-time. 

## **Playing Yahtzee approach 2:**

I gave the prompt “When I say, roll you can give me the numbers which were generated and based on those result I can tell you which ones i wanna have, and other numbers we can re-roll. And you can maintain the scorecard, how about that?” 

AND IT WORKED!! so we jumped right into it! It rolled the virtual dice for me, and I made decisions based on the rolls, choosing which dice to keep and which ones to reroll. Together, we updated the scorecard as I made my moves, aiming for the highest score possible.

## **Implementing Yahtzee:**

I was curious about how to implement Yahtzee in code. Gen-AI guided me through the process, breaking it down into manageable steps and providing pseudocode to outline the structure of the game. We discussed everything from setting up the game to handling player turns and calculating scores.

## **Optimized Functions:**

I wanted optimized functions for each step of the game. So, It crafted efficient functions for initializing players, managing the scorecard, rolling dice, scoring, and updating the scorecard. These functions streamlined the game and made the code more efficient and readable.

## **Exceptional Test Cases:**

I was interested in handling exceptional test cases in the game. We talked about potential issues and how to address them, such as input validation and error handling. Then, it added sample test cases to the code, covering both normal and exceptional scenarios to ensure robustness.

## **Robustness Improvements:**

I asked for suggestions to make the game code more robust. We brainstormed ideas like enhancing input validation, improving error handling, and optimizing performance. It updated the code accordingly, making it more reliable and user-friendly.